package service

import (
	"Main/datasource"
	"Main/utility"
	"context"
	"database/sql"
	"errors"
	"github.com/go-playground/validator/v10"
	gorilla "github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
	"sync"
)

const (
	dbConfPath    = "../resources/configs/datasource.json"
	tableConfPath = "../resources/configs/table.json"
)

var (
	AlreadyExistsError = errors.New("value already exists")
	NotFoundError      = errors.New("value not found")
)

type OrderService struct {
	repo     datasource.OrderRepo
	cache    *sync.Map
	templ    *template.Template
	ctx      context.Context
	validate *validator.Validate
}

func Create(ctx context.Context, templatePath string) (service *OrderService, err error) {
	db, err := datasource.CreateRepo(dbConfPath, tableConfPath)
	if err != nil {
		return
	}

	values, err := db.FindAll(ctx)
	if err != nil {
		return
	}

	cache := sync.Map{}
	for _, val := range values {
		cache.Store(*val.OrderUid, val)
	}

	templ, err := template.ParseFiles(templatePath)
	if err != nil {
		return
	}

	service = &OrderService{datasource.OrderRepo(db), &cache, templ, ctx, validator.New()}
	return
}

func (service *OrderService) AddHandlers(router *gorilla.Router) {
	// no cors policy up to now
	router.HandleFunc("/{id:[a-z0-9]+$}", func(w http.ResponseWriter, r *http.Request) {
		service.HandleExtractValueById(w, r)
	}).Methods(http.MethodGet)
}

func (service *OrderService) Close() error {
	return service.repo.CloseDatabase()
}

func (service *OrderService) loadFromDatabase(id string, ctx context.Context) (*datasource.Order, error) {
	body, err := service.repo.FindOrderById(ctx, id)
	if err != nil {
		if err == sql.ErrNoRows {
			err = NotFoundError
		}
		return nil, err
	}

	log.Println("WARNING: cache miss for " + id)
	service.cache.Store(id, body)

	return body, nil
}

func (service *OrderService) HandleExtractValueById(writer http.ResponseWriter, request *http.Request) {
	id := gorilla.Vars(request)["id"]

	body, err := service.ExtractValueById(id, request.Context())

	if err != nil {
		if err == NotFoundError {
			writer.WriteHeader(400)
		} else {
			writer.WriteHeader(500)
		}
		utility.WriteErrorAsJson(err, writer)
		return
	}

	if err := service.templ.Execute(writer, body); err != nil {
		writer.WriteHeader(500)
		utility.WriteErrorAsJson(err, writer)
		return
	}
}

func (service *OrderService) ExtractValueById(id string, ctx context.Context) (*datasource.Order, error) {
	body, ok := service.cache.Load(id)
	if !ok {
		return service.loadFromDatabase(id, ctx)
	}

	return body.(*datasource.Order), nil
}

func (service *OrderService) InsertValue(order *datasource.Order) error {
	if err := service.validate.Struct(order); err != nil {
		return err
	}

	key := *order.OrderUid

	if _, ok := service.cache.Load(key); ok {
		return AlreadyExistsError
	}

	if err := service.repo.InsertOrder(service.ctx, order); err != nil {
		return err
	}

	service.cache.Store(key, order)
	return nil
}
