package service

import (
	"Main/datasource"
	"Main/utility"
	"encoding/json"
	"fmt"
	"github.com/nats-io/stan.go"
	"log"
	"net"
)

type NatsStreamingConfiguration struct {
	ClusterId string `json:"cluster-id"`
	ClientId  string `json:"client-id"`
	Subject   string
	Ip        net.IP
	Port      uint16
}

const (
	natsConfPath = "../resources/configs/nats.json"
)

// Subscribe создает подписку на Nats Streaming Service. Переденный OrderService используется для сохранения
// объектов
func Subscribe(service *OrderService) (stan.Conn, error) {
	log.Println("Reading nats-streaming service configuration")
	conf, err := utility.ReadJsonFromFile[NatsStreamingConfiguration](natsConfPath)
	if err != nil {
		return nil, err
	}
	log.Println("Configuration is read")

	log.Println("Trying to subscribe to the nats-streaming service")
	sc, err := stan.Connect(conf.ClusterId, conf.ClientId, stan.NatsURL(fmt.Sprintf("nats://%s:%d", conf.Ip, conf.Port)))
	if err != nil {
		return nil, err
	}

	_, err = sc.Subscribe(conf.Subject, func(msg *stan.Msg) {
		order := &datasource.Order{}
		if err := json.Unmarshal(msg.Data, order); err != nil {
			log.Println("WARNING: " + err.Error())
			return
		}

		if err := service.InsertValue(order); err != nil {
			log.Println("WARNING: " + err.Error())
			return
		}
	})

	if err != nil {
		return nil, err
	}

	log.Println("Subscribed")
	return sc, nil
}
