package test

import (
	"Main/datasource"
	"Main/mock"
	"Main/service"
	"Main/utility"
	"log"
	"testing"
)

var orderService *service.OrderService

func beforeEach() {
	if orderService != nil {
		return
	}

	mock.SamplesDir = "../../resources/samples"
	datasource.SetDefaultCreator(mock.MockCreator{})

	var err error
	orderService, err = service.Create(nil, "../../resources/static/index.html")
	if err != nil {
		log.Fatal(err)
	}
}

func TestExtractValueById(t *testing.T) {
	beforeEach()

	val, err := orderService.ExtractValueById("1", nil)

	if err != nil {
		t.Fatal("Error must be nil! Error: " + err.Error())
	}

	expected := mock.Mock["1"]
	if val != expected {
		t.Error("Actual and expected values are different!")
	}
}

func TestExtractValueByIdInvalid(t *testing.T) {
	beforeEach()

	_, err := orderService.ExtractValueById("abcd", nil)
	if err != service.NotFoundError {
		t.Error("Something went wrong " + err.Error())
	}
}

func TestInsertValueValid(t *testing.T) {
	beforeEach()

	value, err := utility.ReadJsonFromFile[datasource.Order]("../../resources/models/valid.json")
	if err != nil {
		t.Fatal("Something went wrong: " + err.Error())
	}

	if err = orderService.InsertValue(value); err != nil {
		t.Error("Something went wrong: " + err.Error())
	}
}

func TestInsertValueInvalid(t *testing.T) {
	beforeEach()

	value, err := utility.ReadJsonFromFile[datasource.Order]("../../resources/models/invalid.json")
	if err != nil {
		t.Fatal("Something went wrong " + err.Error())
	}

	if err = orderService.InsertValue(value); err == nil {
		t.Error("Something went wrong, invalid json was inserted!")
	}

	t.Log(err)
}
