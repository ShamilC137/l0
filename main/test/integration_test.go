package test

import (
	"Main/datasource"
	"Main/service"
	"Main/utility"
	"context"
	"encoding/json"
	"fmt"
	"github.com/nats-io/stan.go"
	"testing"
)

const (
	natsStreamingPath = "../../resources/configs/nats.json"
	modelsPath        = "../../resources/models"
	dbConfPath        = "../../resources/configs/datasource.json"
	tableConfPath     = "../../resources/configs/table.json"
	clientId          = "test-publisher"
)

func fatal(err error, t *testing.T) {
	if err != nil {
		t.Fatal(err)
	}
}

func publish(t *testing.T, path string) *datasource.Order {
	configuration, err := utility.ReadJsonFromFile[service.NatsStreamingConfiguration](natsStreamingPath)
	fatal(err, t)

	sc, _ := stan.Connect(configuration.ClusterId, clientId, stan.NatsURL(fmt.Sprintf("nats://%s:%d", configuration.Ip, configuration.Port)))

	defer sc.Close()

	order, err := utility.ReadJsonFromFile[datasource.Order](path)
	fatal(err, t)

	data, err := json.Marshal(order)
	fatal(err, t)

	fatal(sc.Publish(configuration.Subject, data), t)

	return order
}

func TestValidPublishing(t *testing.T) {
	order := publish(t, modelsPath+"/valid.json")

	repo, _ := datasource.CreateRepo(dbConfPath, tableConfPath)
	table := repo.(*datasource.OrderTable)

	_, err := table.FindOrderById(context.Background(), *order.OrderUid)
	fatal(err, t)

	_, err = table.Db.Exec(fmt.Sprintf("DELETE FROM %s WHERE id = '%s'", table.TableDesc.String(), *order.OrderUid))
	fatal(err, t)
}

func TestInalidPublishing(t *testing.T) {
	publish(t, modelsPath+"/invalid.json")
}
