package mock

import (
	"Main/datasource"
	"Main/utility"
	"context"
	"database/sql"
	"io/ioutil"
)

var SamplesDir string

// concurrent unsafe
type OrderTableMock map[string]*datasource.Order

type MockCreator struct{}

var Mock OrderTableMock

func (creator MockCreator) CreateOrderRepo(_, _ string) (table datasource.OrderRepo, err error) {
	Mock = make(map[string]*datasource.Order)

	if len(SamplesDir) == 0 {
		return Mock, nil
	}

	files, err := ioutil.ReadDir(SamplesDir)
	if err != nil {
		return nil, err
	}

	for _, file := range files {
		value, err := utility.ReadJsonFromFile[datasource.Order](SamplesDir + "/" + file.Name())
		if err != nil {
			return nil, err
		}
		Mock[*value.OrderUid] = value
	}

	return Mock, nil
}

func (mock OrderTableMock) FindOrderById(_ context.Context, id string) (*datasource.Order, error) {
	val, ok := mock[id]
	if !ok {
		return nil, sql.ErrNoRows
	}

	return val, nil
}

func (mock OrderTableMock) FindAll(_ context.Context) ([]*datasource.Order, error) {
	nElems := len(mock)
	if nElems == 0 {
		return nil, sql.ErrNoRows
	}

	result := make([]*datasource.Order, 0, nElems)
	for _, val := range mock {
		result = append(result, val)
	}

	return result, nil
}

func (mock OrderTableMock) InsertOrder(_ context.Context, order *datasource.Order) error {
	mock[*order.OrderUid] = order
	return nil
}

func (mock OrderTableMock) CloseDatabase() error {
	return nil
}
