package utility

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
)

// ReadJsonFromFile читает json из файла в объект указанного типа
func ReadJsonFromFile[T any](filename string) (*T, error) {
	// assert that filename is .json
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	result := new(T)
	err = json.Unmarshal(data, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func LogError(err error) {
	if err != nil {
		log.Println(err)
	}
}

func FatalError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// WriteErrorAsJson пытается записать сообщение ошибки в ответ в виде json. Если не получается,
// логирует причину
func WriteErrorAsJson(err error, writer http.ResponseWriter) {
	writer.Header().Set("Content-Type", "application/json; charset=utf-8")
	writer.Header().Set("X-Content-Type-Options", "nosniff")
	_, internalError := io.WriteString(writer, fmt.Sprintf("{\"error\": \"%s\"}", err))
	LogError(internalError)
}
