package server

import (
	"Main/utility"
	"context"
	"fmt"
	gorilla "github.com/gorilla/mux"
	"log"
	"net"
	"net/http"
	"time"
)

const (
	serverConfPath = "../resources/configs/server.json"

	shutdownTimeout = 15 * time.Second
)

// Configuration используется для настройки сервера
type Configuration struct {
	Ip           net.IP
	Port         int
	WriteTimeout time.Duration
	ReadTimeout  time.Duration
}

// Create создает и настраивает сервера
func Create() (result *http.Server, err error) {
	log.Println("Reading the server configuration file")
	config, err := utility.ReadJsonFromFile[Configuration](serverConfPath)
	if err != nil {
		return
	}
	log.Println("The configuration is read")

	result = &http.Server{
		Addr:         fmt.Sprintf("%s:%d", config.Ip.String(), config.Port),
		Handler:      gorilla.NewRouter().StrictSlash(true),
		ReadTimeout:  config.ReadTimeout,
		WriteTimeout: config.WriteTimeout,
	}

	return
}

// StartServe начинает прослушивание и принятие подключений до тех пор, пока не будет закрыт переданный контекст.
// Переданный контекст также используется для работы с БД
func StartServe(server *http.Server, stop context.Context) error {
	var serverError error
	go func() {
		log.Println("The server is started")
		serverError = server.ListenAndServe()
	}()

	<-stop.Done()

	log.Println("Starting the server shutdown")
	ctx, cancel := context.WithTimeout(context.Background(), shutdownTimeout)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		return err
	}

	if serverError != nil && serverError != http.ErrServerClosed {
		return serverError
	}

	log.Println("The server is down")
	return nil
}
