package datasource

import (
	"Main/utility"
	"context"
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
	"sync"
)

// Configuration настройки подключения к БД
type Configuration struct {
	Driver   string
	Host     string
	Dbname   string
	Port     uint16
	User     string
	Password string
	Sslmode  string
}

// TableDesc используется для описания таблицы
type TableDesc struct {
	Name   string
	Schema string
}

func (desc *TableDesc) String() string {
	return desc.Schema + "." + desc.Name
}

// OrderRepo предоставляет репозиторий со всеми необходимыми действиями
type OrderRepo interface {
	FindOrderById(ctx context.Context, id string) (*Order, error)
	FindAll(ctx context.Context) ([]*Order, error)

	InsertOrder(ctx context.Context, order *Order) error

	CloseDatabase() error
}

// OrderTableCreator объекты данного типа будут использованы для создания репозиториев
type OrderTableCreator interface {
	CreateOrderRepo(dbConfFilepath, tableConfFilepath string) (table OrderRepo, err error)
}

// DefaultCreator будет использоваться для создания репозитория по умолчанию, если не указан другой
type DefaultCreator struct {
	configuration *Configuration
}

var defaultCreator OrderTableCreator = &DefaultCreator{}

// OrderTable отражает отношение
type OrderTable struct {
	Db                 *sql.DB
	TableDesc          *TableDesc
	preparedStatements sync.Map
}

func (creator *DefaultCreator) initDB(config *Configuration) (*sql.DB, error) {
	log.Println("Trying to connect to the database")
	db, err := sql.Open(config.Driver, fmt.Sprintf("host=%s dbname=%s port=%d user=%s password=%s sslmode=%s",
		config.Host, config.Dbname, config.Port, config.User, config.Password, config.Sslmode))
	if err != nil {
		return nil, err
	}

	if err = db.Ping(); err != nil {
		if err := db.Close(); err != nil {
			return nil, err
		}
		return nil, err
	}

	log.Println("Connected to the database")
	return db, nil
}

func (creator *DefaultCreator) createDBObject(configFilepath string) (*sql.DB, error) {
	if creator.configuration == nil {
		log.Println("Reading the database configuration file")
		conf, err := utility.ReadJsonFromFile[Configuration](configFilepath)
		if err != nil {
			return nil, err
		}
		log.Println("Configuration is read")

		creator.configuration = conf
	}

	db, err := creator.initDB(creator.configuration)
	if err != nil {
		return nil, err
	}

	return db, nil
}

func (creator *DefaultCreator) CreateOrderRepo(dbConfFilepath, tableConfFilepath string) (table OrderRepo, err error) {
	db, err := creator.createDBObject(dbConfFilepath)
	if err != nil {
		table = nil
		return
	}

	tableDesc, err := utility.ReadJsonFromFile[TableDesc](tableConfFilepath)
	if err != nil {
		return
	}

	if _, err = db.Exec(fmt.Sprintf("CREATE TABLE IF NOT EXISTS %s.%s (id VARCHAR PRIMARY KEY, data JSON NOT NULL)",
		tableDesc.Schema, tableDesc.Name)); err != nil {
		return
	}

	table = &OrderTable{db, tableDesc, sync.Map{}}
	return
}

func SetDefaultCreator(creator OrderTableCreator) {
	defaultCreator = creator
}

// CreateRepo создает репозиторий, используя установленный OrderTableCreator
func CreateRepo(dbConfFilepath, tableConfFilepath string) (OrderRepo, error) {
	return defaultCreator.CreateOrderRepo(dbConfFilepath, tableConfFilepath)
}

func (source *OrderTable) getPreparedStatement(key, query string, ctx context.Context) (*sql.Stmt, error) {
	stmt, ok := source.preparedStatements.Load(key)
	if !ok {
		val, err := source.Db.PrepareContext(ctx, query)
		if err != nil {
			return nil, err
		}
		source.preparedStatements.Store(key, val)
		return val, nil
	}

	return stmt.(*sql.Stmt), nil
}

func (source *OrderTable) truncatePreparedStatements() error {
	var err error
	source.preparedStatements.Range(func(key any, stmt any) bool {
		if err = stmt.(*sql.Stmt).Close(); err != nil {
			return false
		}
		source.preparedStatements.Delete(key)
		return true
	})
	return err
}

func (source *OrderTable) CloseDatabase() error {
	err := source.truncatePreparedStatements()
	if err != nil {
		return err
	}

	err = source.Db.Close()
	if err != nil {
		return err
	}

	source.Db = nil
	return nil
}
