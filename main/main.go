package main

import (
	"Main/server"
	"Main/service"
	"Main/utility"
	"context"
	gorilla "github.com/gorilla/mux"
	"os"
	"os/signal"
	"sync"
	"syscall"
)

const (
	templatePath = "../resources/static/index.html"
)

func main() {
	const routinesCount uint8 = 1 // количество синхронизуемых объектов
	var shutdownSync = sync.WaitGroup{}
	shutdownSync.Add(int(routinesCount))

	// Общий контекст, используемый для завершения работы сервера
	ctx, cancel := context.WithCancel(context.Background())

	// Создание и настройка сервиса заказов
	orderService, err := service.Create(ctx, templatePath)
	utility.FatalError(err)

	defer func() {
		utility.FatalError(orderService.Close())
	}()

	// Подписка на получение сообщений от Nats Streaming Service
	sc, err := service.Subscribe(orderService)
	utility.FatalError(err)

	defer func() {
		utility.FatalError(sc.Close())
	}()

	// Создание и настройка HTTP сервера
	myserver, err := server.Create()
	utility.FatalError(err)
	orderService.AddHandlers(myserver.Handler.(*gorilla.Router))

	go func() {
		utility.FatalError(server.StartServe(myserver, ctx))
		shutdownSync.Done()
	}()

	// Настройка обработки сигналов
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, syscall.SIGINT, syscall.SIGTERM)

	<-shutdown

	// Завершение работы всех объектов, выполняющихся в других горутинах
	cancel()

	// Ожидания завершения работы синхронизуемых объектов
	shutdownSync.Wait()
}
