package main

import (
	"encoding/json"
	"fmt"
	"github.com/nats-io/stan.go"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"strings"
	"time"
)

const (
	clientId          = "publisher"
	configurationPath = "../resources/configs/nats.json"
	modelsDirPath     = "../resources/models"
	serverConfPath    = "../resources/configs/server.json"
	templatePath      = "../resources/static/index.html"
)

type NatsStreamingConfiguration struct {
	ClusterId string `json:"cluster-id"`
	ClientId  string `json:"client-id"`
	Subject   string
	Ip        net.IP
	Port      uint16
}

type ServerConfiguration struct {
	Ip   net.IP
	Port int
}

func ReadJsonFromFile[T any](filename string) (*T, error) {
	// assert that filename is .json
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	result := new(T)
	err = json.Unmarshal(data, result)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func main() {
	configuration, err := ReadJsonFromFile[NatsStreamingConfiguration](configurationPath)
	fatal(err)

	sc, err := stan.Connect(configuration.ClusterId, clientId, stan.NatsURL(fmt.Sprintf("nats://%s:%d", configuration.Ip, configuration.Port)))
	fatal(err)

	defer sc.Close()

	files, err := ioutil.ReadDir(modelsDirPath)
	fatal(err)

	templ, err := template.ParseFiles(templatePath)
	fatal(err)

	serverConf, err := ReadJsonFromFile[ServerConfiguration](serverConfPath)
	fatal(err)

	builder := new(strings.Builder)
	for _, file := range files {
		object, err := ReadJsonFromFile[Order](modelsDirPath + "/" + file.Name())
		fatal(err)

		data, err := json.Marshal(object)
		fatal(err)

		err = sc.Publish(configuration.Subject, data)
		fatal(err)

		// Вместо подтверждения сервера, что сообщение получено и обработано
		time.Sleep(100 * time.Millisecond)

		var resp *http.Response
		if !strings.Contains(file.Name(), "invalid") {
			resp, err = http.Get(fmt.Sprintf("http://%s:%d/%s", serverConf.Ip, serverConf.Port, *object.OrderUid))
			fatal(err)

			err = templ.Execute(builder, object)
			fatal(err)

			actual, err := io.ReadAll(resp.Body)
			fatal(err)

			if builder.String() != string(actual) {
				log.Println("Expected: " + builder.String())
				log.Println("Actual: " + string(actual))
				log.Fatal("Expected and actual result are differ. Order Uid: " + *object.OrderUid)
			}

			builder.Reset()
		}

	}
}
